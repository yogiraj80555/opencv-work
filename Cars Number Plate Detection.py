import cv2
import numpy as np

frameWidht = 640
frameHeight = 480





cap = cv2.VideoCapture(0)
cap.set(3,frameWidht)
cap.set(4,frameHeight)

minArea = 500
#cascading object
numberPlateCascade = cv2.CascadeClassifier("haarcascade_russian_plate_number.xml")
try:
    while True:
        _,img = cap.read();
        
        imgGray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        #here we ar using cascadeing 
        plates = numberPlateCascade.detectMultiScale(imgGray, 1.1, 4)
        for (x, y, w, h) in plates:
            if (x*y) > minArea:
                cv2.rectangle(img, (x,y), (x+w, y+h), (255,105,250), 2)
                cv2.putText(img, "Number Plate", (x, y-5),
                           cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255,255,0),2)
                #so here we extracting region of image which is number plate
                plate = img[y+10:y+h, x+10:x+w]
                ims = cv2.resize(plate,(300,200))
                cv2.imshow("Number Plate", ims)
        
        
        cv2.imshow("Orignal Image: ",img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break;
        
finally:
    cap.release()
    cv2.destroyAllWindows()