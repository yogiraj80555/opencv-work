#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

int mains() {
	string path = "Resources/building.jpg";
	Mat img = imread(path);
	imshow("Image", img);
	waitKey(0);
	return 0;
}