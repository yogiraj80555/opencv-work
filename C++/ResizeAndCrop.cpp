#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>


int main() {

	std::string path = "Resources/cards.png";
	cv::Mat img = cv::imread(path);
	cv::Mat imgResize, imgCrop;

	std::cout << img.size() << std::endl;
	//cv::resize(img, imgResize, cv::Size(620,460)); //with this we not able to maintain aspect ratio so see below
	cv::resize(img, imgResize, cv::Size(), 0.8, 0.8);    //so this will scale in exact pixels
	//above image decrease at level 20%


	//image Cropping we required it for "Region of interest" we know as 'ROI'
	cv::Rect roi(100, 100, 300, 250);
	imgCrop = img(roi);





	cv::imshow("Image", img);
	imshow("Sesize image", imgResize);
	imshow("Crop Image", imgCrop);

	cv::waitKey(0);


	return 0;
}