#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

float width = 250, height = 350;

int main() {

	std::string path = "Resources/cards.png";
	cv::Mat img,matrix, imgWrap;
	img = cv::imread(path);
	//top left 119,178      top right 195,216
	//bottom left 62,293    bottom right 139,330

	cv::Point2f src[4] = { {118,179},{192,219},{62,293},{139,330} };
	cv::Point2f destination_src[4] = { {0.0f,0.0f},{width,0.0f},{0.0f,height},{width,height} };


	//here we using transformation matrix to find the warp
	matrix = cv::getPerspectiveTransform(src, destination_src);
	cv::warpPerspective(img, imgWrap, matrix, cv::Point(width, height));

	//creating a circles around points in 'src' we used
	for (int i = 0; i < 4; i++) {
		cv::circle(img, src[i], 5, cv::Scalar(100, 0, 5), -1);
	}



	cv::imshow("Cards", img);
	cv::imshow("Image Wrap", imgWrap);
	cv::waitKey(0);

	return 0;
}