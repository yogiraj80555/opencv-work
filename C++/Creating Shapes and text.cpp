#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>


int main() {
	std::string path = "Resources/apple.jpg";
	cv::Mat img = cv::imread(path);
	std::cout << img.size() << std::endl;
	cv::Mat blnkImg(512, 512, CV_8UC3, cv::Scalar(255, 0, 0)); //here CV_8UC3 stands for CV  8U for 8 bit unsigned maxis 255, C3 for 3 channel
	//so it becomes 255*3 among them in forst channel we assign 255 for all values it's BGR so image become blue
	//for black make scaler value 0,0,0


	//cv::circle(blnkImg, cv::Point(256, 256), 155, cv::Scalar(0, 69, 255),10); //last value for circle boundry line thickness
	//If you want filled cicle then below
	cv::circle(blnkImg, cv::Point(256, 256), 155, cv::Scalar(0, 69, 255),-1); //for filled circle use cv::FILLED or -1

	cv::rectangle(blnkImg, cv::Point(130, 226), cv::Point(382, 286), cv::Scalar(255, 255, 250), -1);

	cv::line(blnkImg, cv::Point(130, 296), cv::Point(382, 296), cv::Scalar(255, 255, 220), 4);


	cv::putText(blnkImg, "OpenCV Learning", cv::Point(137, 262), cv::FONT_HERSHEY_DUPLEX, 0.85, cv::Scalar(5, 87, 20), 2);
	imshow("imahge", blnkImg);
	cv::waitKey(0);

	return 0;
}
