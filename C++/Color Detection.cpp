#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include<opencv2/imgcodecs.hpp>




int hueMin = 0, satMin = 0, vMin = 0;
int hueMax = 255, satMax = 255, vMax = 180;


void detectColor_Trackbar(cv::Mat img, cv::Mat imgHSV) {

	cv::Mat imgMask;
	//creating trackbar
	cv::namedWindow("Trackbars", (640, 750)); 
	cv::createTrackbar("Hue Min", "Trackbars", &hueMin, 180); //Red 146   //blue 78  //green 36
	cv::createTrackbar("Hue Max", "Trackbars", &hueMax,  180); //Red 180	//blue 110	//green 56
	cv::createTrackbar("Saturation Min", "Trackbars", &satMin, 255); //Red 158	//blue 216	//green 100
	cv::createTrackbar("Saturation Max", "Trackbars", &satMax, 255); //Red 218	//blue 250	//green 137
	cv::createTrackbar("Value Min", "Trackbars", &vMin, 255);  //Red 0	//blue 0	//green 216
	cv::createTrackbar("Value Max", "Trackbars", &vMax, 255);  //Red 255	//blue 255	//green 221

	while (true) {
		//Min range
		cv::Scalar lower(hueMin, satMin, vMin);

		//Max Range
		cv::Scalar upper(hueMax, satMax, vMax);

		//using inrange function to collect color
		cv::inRange(imgHSV, lower, upper, imgMask);
		//Remember here we usually we define directly what color we want but in reality because of lighting and different shadow 
		//the color not be exactly one value so it will be range of values its always good to use that range
		//so here  we using 'lower' and 'upper' for an range

		cv::imshow("Image", img);
		cv::imshow("Image HSV", imgHSV);
		cv::imshow("Image Mask", imgMask);
		cv::waitKey(1);
	}
}





int main() {

	std::string path = "Resources/opencv-logo.png";
	cv::Mat img, imgHSV, imgMask;
	img = cv::imread(path);
	

	//conver image in HSV space
	cv::cvtColor(img, imgHSV, cv::COLOR_BGR2HSV);

	//using Track bar for detecting color
	//detectColor_Trackbar(img,imgHSV);
	//comment above function when work done

	//for Blue Color
	hueMin = 78; hueMax = 110; satMin = 216; satMax = 250; vMin = 0; vMax = 255;
	//for Red Color
	//hueMin = 146; hueMax = 180; satMin = 158; satMax = 218; vMin = 0; vMax = 255;
	//for Green Color
	//hueMin = 36; hueMax = 56; satMin = 100; satMax = 137; vMin = 216; vMax = 221;




	//Min range
	cv::Scalar lower(hueMin, satMin, vMin);

	//Max Range
	cv::Scalar upper(hueMax, satMax, vMax);

	//using inrange function to collect color
	cv::inRange(imgHSV, lower, upper, imgMask);
	//Remember here we usually we define directly what color we want but in reality because of lighting and different shadow 
	//the color not be exactly one value so it will be range of values its always good to use that range
	//so here  we using 'lower' and 'upper' for an range

	imshow("Image", img);
	imshow("Image HSV", imgHSV);
	imshow("Image Mask", imgMask);
	cv::waitKey(0);

	return 0;
}