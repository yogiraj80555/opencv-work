#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>


//using namespace cv;
//using namespace std;



int main() {

	std::string path = "Resources/building.jpg";
	cv::Mat img = cv::imread(path);
	cv::Mat imgGray, imgBlur, imgCanny, imgDilate, imgErode;

	//converting Gray
	cv::cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);

	//Adding the blur
	//GaussianBlur(img,img)
	cv::GaussianBlur(img, imgBlur, cv::Size(5, 5), 5, 0);

	//Edge Dectetor Canny
	cv::Canny(imgBlur, imgCanny, 25, 80);

	//Dilute and erode an image
	//When we detect image it not completely filled or they are very thin to detect properly we can dilate it so it increase its thickness
	//and wee erode it to decrease its thickness
	cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));//increase size dilete more decrease size dilate less
	cv::dilate(imgCanny, imgDilate, kernel);

	cv::erode(imgDilate, imgErode, kernel);



	imshow("Image", img);
	imshow("Gray Image", imgGray);
	imshow("Gaussian Blur Image", imgBlur);
	imshow("Canny Edge Detector", imgCanny);
	imshow("Image Dilation", imgDilate);
	imshow("Image Erodation", imgErode);
	cv::waitKey(0);
	return 0;


}