#include<opencv2/imgproc.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgcodecs.hpp>
#include<iostream>




void getContours(cv::Mat dilateImg, cv::Mat orgImg) {

	//so below is vector inside this we have more vectors and this each vector there will be contour and each contour have points
	/*
		{ {cv::Point(20, 30), cv::Point(50, 60)  },
			{ cv::Point(), cv::Point() },
			{ cv::Point(), cv::Point() }
		};
		see below contour vector
	*/
	std::vector<std::vector<cv::Point>> contours; 
	std::vector<cv::Vec4i> hierarchy;  //here Vec4i means 4 integer values    
	
	cv::findContours(dilateImg, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
	//cv::drawContours(orgImg, contours, -1, cv::Scalar(255, 52, 250), 2);

	  

	/*
	cv::findContours(dilateImg, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);
	//till here we can find the contours 
	cv::drawContours(orgImg, contours, contours.size() - 1, cv::Scalar(255, 53, 255), 2);
	

	//thear are might me nested shapes soo we are using all Retrival Modes in find contours
	//for more please try contour Approximation modes also in findContours 
	cv::findContours(dilateImg, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
	cv::drawContours(orgImg, contours, contours.size() - 1, cv::Scalar(255, 53, 255), 2);
	
	
	cv::findContours(dilateImg, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
	cv::drawContours(orgImg, contours, contours.size() - 1, cv::Scalar(255, 53, 255), 2);
	

	
	cv::findContours(dilateImg, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_TC89_KCOS);
	cv::drawContours(orgImg, contours, contours.size() - 1, cv::Scalar(255, 53, 255), 2);
	*/


	//finding out are and filter out wrong detected data
	


	std::vector<std::vector<cv::Point>> contoursShape(contours.size());
	std::vector<cv::Rect> boundRect(contours.size());
	std::string shapeName = "Default Name";
	for (int i = 0; i < contours.size(); i++) {

		int area = cv::contourArea(contours[i]);
		//std::cout << "Size of vector: " << contours[i].size() << std::endl;
		std::cout << area << "        " << contours[i][0] <<std::endl;
		if (contours[i][0].x > 10 && area > 500) {
			//comment above draw
			//
			//till here we done filteration of our area
			//from here we started binding obx of our objects/shapes
			float perimeter = cv::arcLength(contours[i], true);
			//here we find number of curvs let's say it has four curves then it's reactangle if 3 then triangle and if it has lots of probably its an circle
			cv::approxPolyDP(contours[i], contoursShape[i], 0.01 * perimeter, true);
			//
			std::cout << contoursShape[i].size() << std::endl<<"\n";
			boundRect[i] = cv::boundingRect(contoursShape[i]);
			//cv::rectangle(orgImg, boundRect[i].tl(), boundRect[i].br(), cv::Scalar(0, 255, 0), 5);  //this is our idea
			//here we find which object is which
			int objCorner = (int)contoursShape[i].size();
			
			
			if (objCorner == 2)
				shapeName = "Tri";
			else if (objCorner == 4)
				shapeName = "Rect";
			else if (objCorner > 4)
				shapeName = "Circle";


			cv::drawContours(orgImg, contoursShape, i, cv::Scalar(255, 0, 255), 2);
			//cv::rectangle(orgImg, boundRect[i].tl(), boundRect[i].br(), cv::Scalar(0, 255, 0), 5);
			//cv::putText(orgImg, shapeName, { boundRect[i].x , boundRect[i].y - 5 }, cv::FONT_HERSHEY_DUPLEX, 0.75, cv::Scalar(0, 0, 255), 1); //currently detecting wrong names

		}



	}

}






int main() {
	std::string path = "Resources/Shape.JPG";
	//path = "Resources/pic5.png";
	

	cv::Mat img,imgGray,imgBlur,imgCanny, imgDilate, imgErode;
	img = cv::imread(path);

	//preprcessing the image before finding conturs/shapes
	

	//idea is if we find the edges if image then we find which shape of that edge
	//we use canney edge detector to first find the edges then we find countour points
	cv::cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);
	cv::GaussianBlur(imgGray, imgBlur, cv::Size(3, 3), 3, 0);
	cv::Canny(imgBlur, imgCanny, 25, 70);

	//using little bit diluation and errosion
	cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
	cv::dilate(imgCanny, imgDilate, kernel);
	//cv::erode(imgDilate, imgErode, kernel);


	//finding contours
	getContours(imgDilate, img);


	imshow("Image", img);
	//imshow("Gray Image", imgGray);
	//imshow("Blur Image", imgBlur);
	//imshow("Canny Image", imgCanny);
	//imshow("Dilate Image", imgDilate);
	////imshow("Erode Image", imgErode);
	cv::waitKey(0);

	return 0;
}