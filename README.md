This is an Open CV programs created by me during exploring Open CV and thanks for murtaja for giving such simple guideline's. it's a lot's appreciated.

The programs is based on multiple OpenCV codes so we can understand how Open CV work on images. If you want to learn more about Open CV example you can visit about perticular language folder like <li>"Python"</li>

Below you can see some use images of running Codes
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">



## Document Scanner

<figure class="figure mb-3">
  <img src="https://www.patilyogiraj.ml/OtherWork/OpenCV/doc_scanner.jpg" width="650" height="380" class="figure-img img-fluid rounded" alt="Document Scanners">
  <figcaption class="figure-caption"><b><i>Document Scanners</b></i></figcaption>
</figure>
In Above image you can see you can hold your document in any angle program will detect and arrange document image in a portrait size. Excuse about my webcam image quality which is pretty bad. Here the <li>'Result'</li> window will automatically change size in to proper portrait size when programe detect/scan document in image, Otherwise both the image will remain on same size and show same Output also.

## Cars Number Plate Detection
<div class="row">
	<div class="col-6">
		<figure class="figure mb-3">
	  		<img src="https://www.patilyogiraj.ml/OtherWork/OpenCV/NumberPlate_1.jpg" width="650" height="380" class="figure-img img-fluid rounded" alt="Number Plate">
	  		<figcaption class="figure-caption"><b><i>Number Plate Detection 1</b></i></figcaption>
	  	</figure>
	</div>
</div>
<div class="row">
<div class="col-6">
		<figure class="figure mb-3">
	  		<img src="https://www.patilyogiraj.ml/OtherWork/OpenCV/Number_Plate_2.jpg" width="650" height="380" class="figure-img img-fluid rounded" alt="Number Plate">
	  		<figcaption class="figure-caption"><b><i>Number Plate Detection 2</b></i></figcaption>
		</figure>
	</div>
</div>
Here we used <li>haarcascade_russian_plate_number.xml</li> predefined Open CV model to detect number plate on various cars one of above is real time Image which we took during romming in area.
</b>
<br/>
<br/>
<figure class="figure mb-3">
  <img src="https://www.codewars.com/users/Error!!!!/badges/large"  class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>CodeWars Badges</b></i></figcaption>
</figure>
<br/>
Hope you like my work.
<br/>
For any query, questions and suggestions please feel free to contact on below mail id.<br/>
Thankyou. <br/>
By Yogiraj Patil <br/>
Mail: yogiraj.218m0064@viit.ac.in<br/>
To Know <a href="https://yogiraj80555.github.io/YogirajPortfolio/">About me</a> 