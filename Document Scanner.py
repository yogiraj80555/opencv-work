import cv2
import numpy as np

frameWidth = 640
frameHeight = 500

cap = cv2.VideoCapture(0)
cap.set(3,frameWidth)
cap.set(4,frameHeight)
#cap.set(10,150)

##littlebit image preprocessing
def preProcessing(img):
    imgGray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    imgBlur = cv2.GaussianBlur(imgGray,(5,5),1)
    imgCanny = cv2.Canny(imgBlur,200,200)
    #some time edges is to thin so we use dilation and again errosion to make it thicker again
    #2 passes of dilation and 1 for errosion
    kernel = np.ones((5,5))
    imgDial = cv2.dilate(imgCanny,kernel,iterations=1)
    imgErode = cv2.erode(imgDial,kernel,iterations=1)
    
    return imgErode
    
#getting Contours
def getContours(img):
    contours, hir = cv2.findContours(img,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    maxArea = 0
    biggest = np.array([])
    for cntr in contours:
        area = cv2.contourArea(cntr)
        
        if area > 5000:
            #cv2.drawContours(imgCopy, cntr, -1,(255,125,0),3)
            perimeter = cv2.arcLength(cntr,True)
            
            approx = cv2.approxPolyDP(cntr,0.02*perimeter, True)
            #print("Approx is: ",len(approx))
            if len(approx) == 4 and area > maxArea:
                biggest = approx
                maxArea = area
                
    cv2.drawContours(imgCopy, biggest, -1,(255,125,0),5)
    return biggest
                

def reorder(myPoints):
    #when image detect propely it return shape (4,1,2)
    #below we reducing shape to (4,2) only
    newPoints = []
    if len(myPoints) > 0:
        points = myPoints.reshape((4,2))
        newPoints = np.zeros((4,1,2),np.int16)
        adding = points.sum(1)
        print("Adder:- " ,adding,"\n\n\n")
        
        
        newPoints[0] = points[np.argmin(adding)] 
        newPoints[3] = points[np.argmax(adding)]
        
        #finding differentiation for middle points
        diff = np.diff(points,axis = 1)
        newPoints[1] = points[np.argmin(diff)]
        newPoints[2] = points[np.argmax(diff)]
        #print("AgrMax:- " ,np.argmax(adding),"\tArgMin: ",np.argmin(adding),"\n","Diff: ",diff,"\n\n") 
        #print("Points are: ",newPoints,"\n\n\n")
    return newPoints


def perspective(img, biggest):
    
    print(biggest.shape)
    points = np.float32(biggest)
    points1 = np.float32([[0,0], [frameWidth,0], [0,frameHeight],[frameWidth,frameHeight]])
    matrix = cv2.getPerspectiveTransform(points, points1)
    imgout = cv2.warpPerspective(img,matrix,(frameWidth, frameHeight))
    return imgout;

try:
    while True:
        _,img = cap.read()
        cv2.imshow("Orignal Image",img)
        img = cv2.resize(img,(frameWidth,frameHeight))
        imgCopy = img.copy()
        imgThreshold = preProcessing(img)
        biggestSquare = getContours(imgThreshold) ##remember points can come any sequence like index 0 contain x2,y2 next execuation index 0 contain x,y and so on
        ##so x and y values are change based on angle of paper and contour as well or some times we get empty list also
        ## so reorder it
        bigpointArrange = reorder(biggestSquare)

        
        #now sent this for word prespective
        
        if len(bigpointArrange) > 0:
            imgOut = perspective(img,bigpointArrange)
            ims = cv2.resize(imgOut,(480,700))
            cv2.imshow("Result",ims)
        else:
            cv2.imshow("Result",imgCopy)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break; 
        
finally:    
    cap.release()
    cv2.destroyAllWindows()